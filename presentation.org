#+TITLE: Módulo 1: Hardware
#+TITLE:
#+OPTIONS: toc:nil num:nil date:nil author:nil timestamp:nil
#+REVEAL_THEME: solarized

* Hardware
[[file:img/QR_code_hard.png]]
* ¿Qué es una computadora?
@@html:<iframe width="560" height="315" src="https://www.youtube.com/embed/xL8C5CIxDts" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>@@
* ¿Cómo funciona una computadora?
@@html:<iframe width="560" height="315" src="https://www.youtube.com/embed/oYxE3L-6-a8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>@@
* Arquitectura
[[file:img/Arquitectura.png]]
* Componentes principales de una computadora
** CPU: Unidad Central de Procesamiento / Procesador
#+ATTR_HTML: :width 400  :style float:left;margin:20px
[[https://images.idgesg.net/images/article/2018/06/intel_generic_cpu_background-100760187-large.jpg]]
#+ATTR_HTML:    :style float:right;margin:20px
#+REVEAL_HTML: <div style="font-size: 60%;">
*Características:* Velocidad (GHz) - número de núcleos
| Modelo              | Velocidad [GHz] | Núcleos/Hilos) |
|---------------------+-----------------+----------------|
| Intel Core i3-7020U |            2.30 | 2/4            |
| Core i7-11850       |            4.70 | 8/16           |
| Kirin650            |   4x2.0 + 4x1.7 | 8              |
| AMD A4              |             2.5 | 2/4            |
#+REVEAL_HTML: </div>

** Memoria (RAM)
#+ATTR_HTML: :width 400  :style float:left;margin:20px
[[https://www.adslzone.net/app/uploads-adslzone.net/2019/01/memoria-ram-ddr5.jpg]]
#+ATTR_HTML:    :style float:right;margin:20px
#+REVEAL_HTML: <div style="font-size: 60%;">
*Características:* capacidad (GB) - velocidad (MHz)
| Tipo | Velocidad [MHz] | Tasa de transferencia [GB/s] |
|------+-----------------+------------------------------|
| DDR  |             400 |                          3.2 |
| DDR2 |             800 |                          6.4 |
| DDR3 |            1866 |                         14.9 |
| DDR4 |            3200 |                         25.6 |
#+REVEAL_HTML: </div>

** Discos
#+ATTR_HTML:    :style float:left;margin:20px
HDD
#+ATTR_HTML: :width 350 :caption HDD
[[https://cdn2.expertreviews.co.uk/sites/expertreviews/files/0/81/seagate_desktop_hdd.jpg]]

#+ATTR_HTML:  :style float:right;margin:20px
SSD
#+ATTR_HTML: :width 350 :caption SSD
https://itexperts.co.za/wp-content/uploads/2016/01/ssd2.jpg

** Placa de video (GPU)
#+ATTR_HTML: :width 400  :style float:left;margin:20px
https://www.bhphotovideo.com/images/images2500x2500/geforce_900_12005_2500_001_gtx_770_graphics_card_1166261.jpg
#+ATTR_HTML:    :style float:right;margin:20px
#+REVEAL_HTML: <div style="font-size: 60%;">
*Características:* Velocidad (GHz) - número de núcleos
| Modelo                 | Memoria [MB] | Núcleos | GFLOPS |
|------------------------+--------------+---------+--------|
| NVIDIA GTX970          |         4096 |      56 |   3494 |
| NVIDIA GTX960          |         2048 |      32 |   2308 |
| Intel® HD Graphics 630 |     64GB Max |      24 |    768 |
#+REVEAL_HTML: </div>


** Placa madre (Motherboard)
https://images.saymedia-content.com/.image/t_share/MTc1MDA5NjY4MTc4MjU3NjQw/the-motherboard-components.png

** Fuente (Power Supply)
#+ATTR_HTML: :height 400 :style float:left;margin:20px
https://n4.sdlcdn.com/imgs/a/w/h/Astrum-Computer-Power-Supply-450-SDL073294099-1-04d97.jpg
#+REVEAL_HTML: <div>
*Características:*

- Compatibilidad
- Potencia (W)
#+REVEAL_HTML: </div>


** Dipositivos de entrada/salida: Monitor
#+ATTR_HTML: :height 300 :style float:left;margin:20px
[[https://www.tricksladder.com/wp-content/uploads/2015/04/monitor-lcd-diagonal.jpg]]
#+REVEAL_HTML: <div>
*Tamaño:* 12" - 15" - 15,6" - 17,3"
*Resolución:* 800x600 - 1024x768 - 1280x720(HD) - 1920x1080(FHD)
*Conexión:*
#+ATTR_HTML: :width 450
[[https://notebooks.com/wp-content/uploads/2016/03/HDMI-vs-DVI-vs-VGA.jpg]]
#+REVEAL_HTML: </div>

** Dipositivos de entrada/salida: Teclado
#+ATTR_HTML: :height 400 :style float:left;margin:20px
[[https://ae01.alicdn.com/kf/HTB1e98vdY3nBKNjSZFMq6yUSFXaF/14-inch-TPU-laptop-Keyboard-Cover-Protector-Transparent-for-Asus-Vivobook-S14-S410-S410UN-S410ua-S410uq.jpg]]
#+ATTR_HTML: :height 400 :style float:right;margin:20px
[[https://ae01.alicdn.com/kf/HTB1NkUtvaQoBKNjSZJnq6yw9VXa1/15-6-inch-laptop-keyboard-cover-skin-for-Dell-Inspiron-15-5000-series-5545-5547-5548.jpg]]

** Dipositivos de entrada/salida: varios
#+ATTR_HTML: :height 200 :style float:left;margin:20px
[[https://h20386.www2.hp.com/AustraliaStore/Html/Merch/Images/c04328601_1750x1285.jpg]]
#+ATTR_HTML: :height 200 :style float:left;margin:20px
[[https://images.idgesg.net/images/article/2019/05/amazonbasics-computer-speakers-100795879-large.jpg]]
#+ATTR_HTML: :height 200 float:left;margin:20px
[[https://www.modchipcentral.com/store/images/P/9077-3.png]]
#+ATTR_HTML: :height 200 :style float:right;margin:20px
[[https://www.clickbd.com/global/classified/item_img/1045163_0_original.jpg]]
#+ATTR_HTML: :height 200 :style float:right;margin:20px
https://www.slrlounge.com/wp-content/uploads/2018/03/wacom-intuos-get-creative-pen-tablet-new-2018-9.jpg
#+ATTR_HTML: :height 200 :style float:right;margin:20px
[[https://www.powerhouse.je/images/epson-xp-5105-inkjet-printer-p16775-47991_image.jpg]]
